<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="kuutyou-content-info1">
		<img src="<?php bloginfo('template_url'); ?>/img/content/house_content_img1.jpg" alt="house" />
		<table class="info1-text">
			<tr>
				<th>1ルーム</th>
				<td>15,000円～</td>
			</tr>
			<tr>
				<th>1LDK</th>
				<td>20,000円～</td>
			</tr>
			<tr>
				<th>2LDK</th>
				<td>25,000円～</td>
			</tr>
		</table>
	</div>
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="main-title">ハウスクリーニング</h2>	
	<div class="kuutyou-content-info2"><!-- begin kuutyou-content-info2 -->		
		<div class="message-right message-323 clearfix"><!-- begin message-323 -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/house_content_img2.jpg" alt="house" />
			</div><!-- end image -->
			<div class="text ln15em">
				<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
				<p class="pt20">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキス</p>
			</div><!-- end text -->
		</div><!-- end message-323 -->		
	</div><!-- end kuutyou-content-info2 -->	
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2_title">ハウスクリーニングのサービス</h2>
	<div class="message-group message-classic clearfix"><!-- begin message-group -->
        <div class="message-row clearfix"><!--message-row -->
            <div class="message-col message-col243 clearfix"><!-- begin message-col243 -->
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">						
						<h3 class="title-info">キッチン</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/house_content_img3.jpg" alt="house" /></p>
					<div class="info3-text ln15em">
						<p>レンジの油汚れなどをプロの技術で綺麗にします。</p>
					</div>
				</div>								
            </div><!-- end message-col243 -->            
						
			<div class="message-col message-col243 clearfix"><!-- begin message-col243 -->				
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">						
						<h3 class="title-info">フローリング</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/house_content_img4.jpg" alt="house" /></p>
					<div class="info3-text ln15em">
						<p>ワックスを塗布してお部屋を美しくします。</p>
					</div>
				</div> 			
            </div><!-- end message-col243 -->			
			<div class="message-col message-col243 clearfix"><!-- begin message-col243 -->				
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">						
						<h3 class="title-info">換気扇</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/house_content_img5.jpg" alt="house" /></p>
					<div class="info3-text ln15em">
						<p>油汚れや黒ずみなど掃除いたします。</p>
					</div>
				</div>                
            </div><!-- end message-col243 -->
        </div><!-- end message-row -->
		
		<div class="message-row clearfix"><!--message-row -->
            <div class="message-col message-col243 clearfix"><!-- begin message-col243 -->
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">						
						<h3 class="title-info">トイレ</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/house_content_img6.jpg" alt="house" /></p>
					<div class="info3-text ln15em">
						<p>トイレもプロの技術で徹底的に美しく。</p>
					</div>
				</div>								
            </div><!-- end message-col243 -->            
						
			<div class="message-col message-col243 clearfix"><!-- begin message-col243 -->				
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">						
						<h3 class="title-info">窓ガラス</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/house_content_img7.jpg" alt="house" /></p>
					<div class="info3-text ln15em">
						<p>意外に汚れている窓ガラスを掃除いたします。</p>
					</div>
				</div> 			
            </div><!-- end message-col243 -->			
			<div class="message-col message-col243 clearfix"><!-- begin message-col243 -->				
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">						
						<h3 class="title-info">ベランダ</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/house_content_img8.jpg" alt="house" /></p>
					<div class="info3-text ln15em">
						<p>なかなか掃除しないベランダを掃除しませんか？</p>
					</div>
				</div>                
            </div><!-- end message-col243 -->
        </div><!-- end message-row -->
    </div><!-- end message-group -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2_title">ハウスクリーニングの料金</h2>
	<table class="kuutyou-content-info4">
		<tr>
			<th>1ルーム</th>
			<td>8,000円～</td>
		</tr>
		<tr>
			<th>1LDK</th>
			<td>20,000円～</td>
		</tr>
		<tr>
			<th>2LDK</th>
			<td>25,000円～</td>
		</tr>
		<tr>
			<th>3LDK</th>
			<td>35,000円～</td>
		</tr>
		<tr>
			<th>4LDK</th>
			<td>45,000円～</td>
		</tr>
	</table>
</div><!-- end primary-row -->


<?php get_template_part('part','flow'); ?>
<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>