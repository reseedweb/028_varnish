
                                </main><!-- end primary -->
                                <aside class="sidebar"><!--begin sidebar -->
                                    <?php if(is_page('blog') || is_category() || is_single()) : ?>
                                        <?php
                                        $queried_object = get_queried_object();                                
                                        $sidebar_part = 'blog';
                                        if(is_tax() || is_archive()){                                    
                                            $sidebar_part = '';
                                        }                               

                                        if($queried_object->post_type != 'post' && $queried_object->post_type != 'page'){
                                            $sidebar_part = '';
                                        }   
                                                
                                        if($queried_object->taxonomy == 'category'){                                    
                                            $sidebar_part = 'blog';
                                        }                 
                                        ?>
                                        <?php get_template_part('sidebar',$sidebar_part); ?>  
                                    <?php else: ?>
                                        <?php get_template_part('sidebar'); ?>  
                                    <?php endif; ?>                                    
                                </aside>
                            </div><!-- end two-cols -->
                        </div><!-- end col -->
                    </div><!-- end row -->
                </div><!-- end container -->            
            </section><!-- end content -->

            <footer><!-- begin footer -->
                <div class="container"><!-- begin container -->
                    <div class="row clearfix"><!-- begin row -->
                        <div class="col-md-18"><!-- begin col -->
                            <div class="footer-content clearfix">
                                <div class="footer-logo">
									<a href="<?php bloginfo('url'); ?>/">
										<img src="<?php bloginfo('template_url'); ?>/img/common/footer_logo.jpg" alt="footer" />
									</a>
								</div>
								<div class="footer-contact ln2em">
									〒567-0887 大阪府茨木市西中条町3-41-202<br />
									TEL :072-622-5887／FAX :072-626-7475
								</div>
                            </div>
							<div class="footer-navi clearfix">
								<ul class="clearfix">
									<li>
										<a href="<?php bloginfo('url'); ?>/"><i class="fa fa-circle-o"></i>トップページ</a>
									</li>
									<li>
										<a href="<?php bloginfo('url'); ?>/yukumen"><i class="fa fa-circle-o"></i>床面清掃</a>
									</li>
									<li>
										<a href="<?php bloginfo('url'); ?>/aircon"><i class="fa fa-circle-o"></i>エアコンクリーニング</a>
									</li>
									<li>
										<a href="<?php bloginfo('url'); ?>/kuutyou"><i class="fa fa-circle-o"></i>空調設備管理</a>
									</li>
									<li>
										<a href="<?php bloginfo('url'); ?>/house"><i class="fa fa-circle-o"></i>ハウスクリーニング</a>
									</li>
								</ul>
								<ul class="last-child clearfix">
									<li>
										<a href="<?php bloginfo('url'); ?>/price"><i class="fa fa-circle-o"></i>価格表</a>
									</li>
									<li>
										<a href="<?php bloginfo('url'); ?>/flow"><i class="fa fa-circle-o"></i>清掃の流れ</a>
									</li>
									<li>
										<a href="<?php bloginfo('url'); ?>/faq"><i class="fa fa-circle-o"></i>よくあるご質問</a>
									</li>
									<li>
										<a href="<?php bloginfo('url'); ?>/company"><i class="fa fa-circle-o"></i>会社概要</a>
									</li>
									<li>
										<a href="<?php bloginfo('url'); ?>/sagyourei"><i class="fa fa-circle-o"></i>清掃作業例</a>
									</li>
									<li>
										<a href="<?php bloginfo('url'); ?>/blog"><i class="fa fa-circle-o"></i>ブログ</a>
									</li>
									<li>
										<a href="<?php bloginfo('url'); ?>/staff"><i class="fa fa-circle-o"></i>スタッフ紹介</a>
									</li>
									<li>
										<a href="<?php bloginfo('url'); ?>/contact"><i class="fa fa-circle-o"></i>お問い合わせ</a>
									</li>
								</ul>
							</div>
							<div class="copyright">
                                Copyright@2014 株式会社バーニッシュ All Rights Reserved.
                            </div>
                        </div><!-- end col -->
                    </div><!-- end row -->
                </div><!-- end container -->            
            </footer><!-- end footer -->            

        </div><!-- end wrapper -->
        <?php wp_footer(); ?>
        <!-- js plugins -->
        <script type="text/javascript">

            jQuery(document).ready(function(){               
                jQuery('.bxslider').bxSlider({
                    /*pagerCustom: '#bx-pager',*/
                    pause : 3000,
                    auto : true,
                });
                jQuery('.darktooltip').darkTooltip({
                    theme : 'light'
                });                
            }); 
        </script>                
    </body>
</html>