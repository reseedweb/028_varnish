<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">会社概要</h2>
		<p class="mb20">ビル&マンション清掃.comを運営している会社についてご紹介いたします。</p>
		<table class="company_table"><!-- begin company_table -->
			<tr>
				<th>会社名</th>
				<td>株式会社バーニッシュ</td>
			</tr>
			<tr>
				<th>創業</th>
				<td>平成19年</td>
			</tr>
			<tr>
				<th>所在地</th>
				<td>〒567-0887 大阪府茨木市西中条町3-41-202</td>
			</tr>
			<tr>
				<th>連絡先</th>
				<td>
					TEL : 072-622-5887 / FAX : 072-626-7475<br />
					（営）9：00～19：00　（年中無休）<br />
					メールアドレス : burnish@osaka.zaq.jp
				</td>
			</tr>
			<tr>
				<th>代表者</th>
				<td>柳澤 昭彦</td>
			</tr>
			<tr>
				<th>従業員数</th>
				<td>53名 （パート・アルバイト含む）</td>
			</tr>
			<tr>
				<th>保有資格</th>
				<td>
					ビルクリーニング技能士<br />
					清掃作業監督者
				</td>
			</tr>
			<tr>
				<th>事業内容</th>
				<td>
					清掃業全般、空調・ビルメンテナンス業<br />
					内装業<br />
					リフォーム業
				</td>
			</tr>						
		</table><!-- end company_table -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">アクセス</h2>
		<div id="company_map">						
			<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3275.6594817251016!2d135.5639912!3d34.81450789999999!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6000e2d613714c13%3A0x25ec33f92607d891!2z5aSn6Ziq5bqc6Iyo5pyo5biC6KW_5Lit5p2h55S677yT4oiS77yU77yR!5e0!3m2!1sja!2sjp!4v1418353580462" width="760" height="300" frameborder="0" style="border:0"></iframe>			
		</div>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">代表からの挨拶</h2>   
		<div class="message-right message-234 clearfix"><!-- begin message-234 -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/company_content_img.jpg" alt="company" />
			</div>
			<div class="text ln15em">
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
				テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
			</div>
		</div><!-- end message-234 -->
	</div><!-- end primary-row -->
<?php get_template_part('part','contact');?>
<?php get_footer(); ?>