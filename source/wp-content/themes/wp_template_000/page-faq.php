<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="primary-row clearfix"><!-- primary-row -->
		<h2 class="h2_title">よくあるご質問</h2>
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				見積もりは無料ですか？
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer ln15em">
					<p>
						もちろん無料です。 フォームから必要事項をご記入いただき、送信して下さい。2営業日以内に返信致します。<br />
						清掃のことで気になることがあれば、お気軽にご相談ください。
					</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->

	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				施工可能なエリアを教えてください。
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer ln15em">
					<p>
						弊社では、大阪市を中心に県内全域と兵庫・和歌山・滋賀など近隣エリアの一部で清掃を行っております。<br />
						その他のエリアについても対応できることがありますので、まずはご相談ください。
					</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				なぜそんなに安くできるの？
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer ln15em">
					<p>
						弊社は大手清掃業者のように中間業者を間に挟まないので、中間マージンや雑費等の料金がかかってきませ<br />
						ん。そのため、サービスを低価格で質を落とさずに提供することができます。
					</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				本当に表示されている価格で清掃してくれるの？
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer ln15em">
					<p>
						ビル&マンション清掃.comでは、サービスの料金と出張費以外の費用は頂きません。<br />
						ただし、駐車スペースがないお客様には駐車場代を別途でご請求させていただいております。
					</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				スタッフの対応が心配なんだけど…
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer ln15em">
					<p>
						ビル&マンション清掃.comでは、社員の教育を徹底しておりますのでご安心下さい。詳細を知りたいという
						方は、スタッフ紹介ページをご覧ください。	
					</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				他の清掃業者との違いは？
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer ln15em">
					<p>
						清掃の作業自体は当店を含め、おそらくどこの業者も似たようなものだと思います。しかし一つひとつの作業
						を丁寧にしていった結果、お客様から「ここまで丁寧にやってくれるんだ！」と信頼を頂いております。
					</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				頼んだ所以外を勧められたり、色々営業を掛けられるのが嫌なんですが…
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer ln15em">
					<p>
						お客様からのご相談以外、こちらから営業等は行いませんので、安心してお問い合わせください。
					</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- primary-row -->
		<div class="faq-content-row clearfix">
			<div class="question-bg">
				万一、作業中に破損やキズがあった時の補償は？
			</div><!-- end question-bg -->
			<div class="answer-bg">
				<div class="answer ln15em">
					<p>
						作業には最新の注意を払っておりますが、破損させてしまった場合、弊社が責任を持って弁償いたします。<br />
						賠償責任保険にも加入しておりますのでご安心ください。
					</p>
				</div>
			</div><!-- end answer-bg -->	
		</div><!-- end faq-content-row -->
	</div><!-- end primary-row -->	
</div><!-- end primary-row -->		
<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>