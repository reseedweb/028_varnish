<?php get_header(); ?>	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">スタッフ紹介</h2>   
		<p>ビル&マンション清掃.com自慢のスタッフをご紹介します。</p>
		<div class="message-right message-252 clearfix"><!-- begin message-252 -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/staff_content_img1.jpg" alt="staff" />
			</div><!-- ./image -->
			<div class="text ln15em">
				<h3 class="staff-title">柳澤 昭彦(やなぎさわ あきひこ)</h3>
				<table class="staff-info">
					<tr>
						<th>役職：</th>
						<td>代表</td>
					</tr>
					<tr>
						<th>趣味：</th>
						<td>料理</td>
					</tr>
					<tr>
						<th>特技：</th>
						<td>早寝早起き</td>
					</tr>
					<tr>
						<th>夢・目標：</th>
						<td>日本一の清掃業者になること</td>
					</tr>
				</table><!-- ./staff-info -->
			</div><!-- ./text -->
		</div><!-- end message-252 -->
		
		<p>【仕事へのこだわり】</p>
		<p class="ln15em">
			清掃に同じ現場はありません。一軒一軒みなさん違います 現状をしっかり調べ、その場所に必要なことやお客様
			の思いを的確に見極め最善の工事をします。 人との繋がりや思いを一番に仕事をする！
		</p>
		
		<div class="message-right message-252 clearfix"><!-- begin message-252 -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/staff_content_img2.jpg" alt="staff" />
			</div><!-- ./image -->
			<div class="text ln15em">
				<h3 class="staff-title">山田 太郎(やまだ たろう)</h3>
				<table class="staff-info">
					<tr>
						<th>役職：</th>
						<td>業務課長</td>
					</tr>
					<tr>
						<th>趣味：</th>
						<td>料理</td>
					</tr>
					<tr>
						<th>特技：</th>
						<td>早寝早起き</td>
					</tr>
					<tr>
						<th>夢・目標：</th>
						<td>日本一の清掃業者になること</td>
					</tr>
				</table><!-- ./staff-info -->
			</div><!-- ./text -->
		</div><!-- end message-252 -->
		
		<p>【仕事へのこだわり】</p>
		<p class="ln15em">
			清掃に同じ現場はありません。一軒一軒みなさん違います 現状をしっかり調べ、その場所に必要なことやお客様
			の思いを的確に見極め最善の工事をします。 人との繋がりや思いを一番に仕事をする！
		</p>
		
		<div class="message-right message-252 clearfix"><!-- begin message-252 -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/staff_content_img3.jpg" alt="staff" />
			</div><!-- ./image -->
			<div class="text ln15em">
				<h3 class="staff-title">山田 隆(やまだ たかし)</h3>
				<table class="staff-info">
					<tr>
						<th>役職：</th>
						<td>工事担当</td>
					</tr>
					<tr>
						<th>趣味：</th>
						<td>料理</td>
					</tr>
					<tr>
						<th>特技：</th>
						<td>早寝早起き</td>
					</tr>
					<tr>
						<th>夢・目標：</th>
						<td>日本一の清掃業者になること</td>
					</tr>
				</table><!-- ./staff-info -->
			</div><!-- ./text -->
		</div><!-- end message-252 -->
		
		<p>【仕事へのこだわり】</p>
		<p class="ln15em">
			清掃に同じ現場はありません。一軒一軒みなさん違います 現状をしっかり調べ、その場所に必要なことやお客様
			の思いを的確に見極め最善の工事をします。 人との繋がりや思いを一番に仕事をする！
		</p>
		
	</div><!-- end primary-row -->
<?php get_template_part('part','contact');?>
<?php get_footer(); ?>