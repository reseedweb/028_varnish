<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->	
	<h2 class="h2_title">お問い合わせからの流れ</h2>	
	<div class="flow-title mt40">
		<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_step1.jpg" alt="flow" />			
		<h3 class="title-info">メール、電話でのお問い合わせ</h3>
	</div><!-- end flow-title -->
	<div class="flow-content clearfix"><!-- begin flow-content -->
		<div class="flow-right flow-222 clearfix"><!-- begin flow-222 -->							
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img1.jpg" alt="flow" />			
			</div><!-- ./image -->
			<div class="text ln15em">
				<p>
					まずはお問合わせフォームよりご連絡ください。<br />
					お急ぎの場合は、お電話でも承っております。
				</p>
			</div><!-- ./text -->						
		</div><!-- end flow-222 -->					
	</div><!-- end flow-content -->
	<div class="text-center">
		<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="flow" />	
	</div>
	
	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<div class="flow-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_step2.jpg" alt="flow" />			
			<h3 class="title-info">日程の打ち合わせ</h3>
		</div><!-- end flow-title -->
		<div class="flow-content clearfix"><!-- begin flow-content -->
			<div class="flow-right flow-222 clearfix"><!-- begin flow-222 -->							
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img2.jpg" alt="flow" />			
				</div><!-- ./image -->
				<div class="text ln15em">
					<p>
						ご連絡をいただいた時点で、見積もりの日程を打合せします。<br />
						平日の昼間は都合が悪いというお客様、夜間の見積もりも承っております。<br />
						遠慮なくお申し付けください。お問合せフォームをご利用のお客様は、折り返し当社からお電話させていただきます。
					</p>
				</div><!-- ./text -->						
			</div><!-- end flow-222 -->					
		</div><!-- end flow-content -->
		<div class="text-center">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="flow" />	
		</div>
	</div><!--  end primary-row -->

	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<div class="flow-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_step3.jpg" alt="flow" />			
			<h3 class="title-info">現地にてお見積り</h3>
		</div><!-- end flow-title -->
		<div class="flow-content clearfix"><!-- begin flow-content -->
			<div class="flow-right flow-222 clearfix"><!-- begin flow-222 -->							
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img3.jpg" alt="flow" />			
				</div><!-- ./image -->
				<div class="text ln15em">
					<p>
						打合せの日時に現地にお伺いいたします。<br />
						現地を見たうえで見積書を提示いたします。
					</p>
				</div><!-- ./text -->						
			</div><!-- end flow-222 -->					
		</div><!-- end flow-content -->
		<div class="text-center">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="flow" />	
		</div>
	</div><!--  end primary-row -->

	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<div class="flow-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_step4.jpg" alt="flow" />			
			<h3 class="title-info">ご契約</h3>
		</div><!-- end flow-title -->
		<div class="flow-content clearfix"><!-- begin flow-content -->
			<div class="flow-right flow-222 clearfix"><!-- begin flow-222 -->							
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img4.jpg" alt="flow" />			
				</div><!-- ./image -->
				<div class="text ln15em">
					<p>
						見積金額に納得いただいたうえで、契約書にサインをいただきます。<br />
						作業に入る日程、時間の打合せをいたします。
					</p>
				</div><!-- ./text -->						
			</div><!-- end flow-222 -->					
		</div><!-- end flow-content -->
		<div class="text-center">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="flow" />	
		</div>
	</div><!--  end primary-row -->

	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<div class="flow-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_step5.jpg" alt="flow" />			
			<h3 class="title-info">作業当日</h3>
		</div><!-- end flow-title -->
		<div class="flow-content clearfix"><!-- begin flow-content -->
			<div class="flow-right flow-222 clearfix"><!-- begin flow-222 -->							
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img5.jpg" alt="flow" />			
				</div><!-- ./image -->
				<div class="text ln15em">
					<p>
						当日、指定された日時に現地にて作業を行います。<br />
						立会いをしたいけど、時間の都合がつかない場合は前日などに、<br />
						カギをお預かりし作業にあたります。<br />
						必要であれば報告書を後日提出いたしますので、遠慮なくお申し付けください。
					</p>
				</div><!-- ./text -->						
			</div><!-- end flow-222 -->					
		</div><!-- end flow-content -->
		<div class="text-center">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="flow" />	
		</div>
	</div><!--  end primary-row -->

	<div class="primary-row clearfix"><!-- begin primary-row -->		
		<div class="flow-title">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_step6.jpg" alt="flow" />			
			<h3 class="title-info">作業</h3>
		</div><!-- end flow-title -->
		<div class="flow-content clearfix"><!-- begin flow-content -->
			<div class="flow-right flow-222 clearfix"><!-- begin flow-222 -->							
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img6.jpg" alt="flow" />			
				</div><!-- ./image -->
				<div class="text ln15em">
					<p>
						作業の仕上がりを確認していただいたら、現金にて代金の支払いをお願いします。<br />
						また、損害保険に加入しておりますので、万が一の事態でも安心です！
					</p>
				</div><!-- ./text -->						
			</div><!-- end flow-222 -->					
		</div><!-- end flow-content -->	
	</div><!--  end primary-row -->	
</div><!--  end primary-row -->

<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>