<?php get_header(); ?>

	<div class="primary-row clearfix">
		<div id="beforeafter-detail-navi">
	       <ul class="beforeafter-detail-navi clearfix">
	           <li><a href="<?php bloginfo('url'); ?>/cat-beforeafter/床面清掃">造成工事・宅地造成</a></li>
	            <li><a href="<?php bloginfo('url'); ?>/cat-beforeafter/エアコンクリーニング">解体工事</a></li>
	            <li><a href="<?php bloginfo('url'); ?>/cat-beforeafter/空調設備管理">舗装工事</a></li>
	            <li><a href="<?php bloginfo('url'); ?>/cat-beforeafter/ハウスクリーニング">駐車場工事</a></li>
	            <li><a href="<?php bloginfo('url'); ?>/cat-beforeafter/その他">外構工事</a></li>                                                 
	        </ul>			
		</div>
	</div>


<div class="pt20 clearfix"><!-- begin primary-row -->
	<?php if(have_posts()): while(have_posts()) : the_post(); ?>
	<div class="primary-row clearfix">
		<div class="beforeafter-detail-content clearfix">
			<div class="beforeafter-title">
				<div class="beforeafter-title"><?php the_title(); ?></div>
				<div class="beforeafter-date"><?php echo get_the_date('Y/m/d', $p->ID); ?></div>
			</div>
			<table class="beforeafter-table-image">
				<tr>						
					<td class="beforeafter-table-detail">
						<?php if( get_field('image_before') ): ?>
							<img src="<?php the_field('image_before'); ?>" />
						<?php else :?>
							<img src="<?php bloginfo('template_url'); ?>/img/content/beforeafter_content_before.jpg" alt="beforeafter" />					
						<?php endif; ?>
					</td>
					<td class="beforeafter-table-detail-arrow"><img src="<?php bloginfo('template_url'); ?>/img/content/beforeafter_detail_arrow.jpg" alt="beforeafter" /></td>
					<td class="beforeafter-table-detail">
						<?php if( get_field('image_before') ): ?>
							<img src="<?php the_field('image_after'); ?>" />
						<?php else :?>
							<img src="<?php bloginfo('template_url'); ?>/img/content/beforeafter_content_after.jpg" alt="beforeafter" />					
						<?php endif; ?>
					</td>
				</tr>										
			</table>							
			<div class="beforeafter-text">				
					<table class="beforeafter-table-text">
						<tr>					
							<th class="beforeafter-table-title">工事種別</th>
							<td><?php @the_terms($p->ID, 'cat-beforeafter'); ?></td>
						</tr>
						<tr>
							<th class="beforeafter-table-title">施工場所</th>
							<td><?php the_field('place_text'); ?></td>
						</tr>
						<tr>
							<th class="beforeafter-table-title">工期</th>
							<td><?php the_field('date_text'); ?></td>
						</tr>					
				</table>
				
				<div class="beforeafter-detail-info">
					<p class="pb10"><img src="<?php bloginfo('template_url'); ?>/img/content/beforeafter_detail_info.jpg" alt="beforeafter" /></p>
					<p><?php the_field('detail_area'); ?></p>
				</div>
			</div>
		</div>
	</div>	

	<div class="primary-row clearfix">	    
		<?php if( get_previous_post() ): ?>		
		<div class="beforeafter-prev-text">
			<?php previous_post_link('%link'); ?>					
		</div>		
		<?php endif;?>		
	</div><!-- end primary-row -->
	
	<?php endwhile;endif; ?>
</div><!-- end primary-row -->

	
	<?php get_template_part('part','flow'); ?>
    <?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>