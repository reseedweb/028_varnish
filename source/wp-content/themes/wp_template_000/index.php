<?php get_header(); ?>


<div class="top-content clearfix"><!-- begin primary-row -->
	<p>
		<img src="<?php bloginfo('template_url'); ?>/img/content/top_content_img1.jpg" alt="top"/>
	</p>
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="main-title">ビル・マンション清掃.comはなぜ安い？</h2>
	<div class="top-content-info2">			
		<img src="<?php bloginfo('template_url'); ?>/img/content/top_content_img2.jpg" alt="top" />
			<div class="title">弊社は中間業者を挟まない自社一貫施工です！</div>
			<div class="text ln15em">					
				弊社では、中間業者を通さずに清掃の仕事を受けておりますので、中間マージン
				や雑費などの余計な金額がかからず、コストを抑える事ができます！
			</div>					
	</div><!-- .top-content-info1 -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="top-content-info3"><!-- begin top-flow-content -->
		<div class="top-info3-title ln15em">
			ビル&マンションの清掃なら<br />
			<span class="title-color">ビル&マンション清掃.com</span>にお任せください！
		</div>
		<ul class="top-point clearfix">
			<li>
				<span class="title">確かな技術をもとにした品質の高い清掃サービス</span>
				<p class="mt10 ln15em">
					当社は創業7年の経験から培った確かな技術をもとに、品質の高い清掃サービスをご提供します。これまで、掃除の品質に関するクレームはほぼゼロで、
					「ここまで丁寧に掃除する業者は見たことない」といった声もよく頂いております。
				</p>
			</li>
			<li>
				<span class="title">大手清掃会社に比べ、割安な価格で承ります</span>
				<p class="mt10">
					当社では、品質は落とさずにできるだけ低価格でサービスをご提供できるよう努めております。大手清掃会社に比べると、割安でお引き受けできるケースも多くございます。<br />
					まずはお気軽にお見積りをご依頼ください
				</p>
			</li>
			<li>
				<span class="title">現地調査無料!</span>
				<p class="mt10 ln15em">
					お客様に気軽にご相談いただけるよう、無料で現地調査も行っています。自社スタッフがお客様のところへ駆けつけて採寸を行い、明確なお見積りを出させていただきます。
				</p>
			</li>
		</ul>  
	</div>
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2_title">清掃作業風景</h2> 
	<div class="message-group message-classic clearfix"><!-- begin message-group -->
        <div class="message-row clearfix"><!--message-row -->
            <div class="message-col message-col370 clearfix">
                <div class="image">
                   <iframe width="370" height="220" src="//www.youtube.com/embed/clcejyi12qw" frameborder="0" allowfullscreen></iframe>
                </div><!-- end image -->                
            </div><!-- end message-col -->
			 <div class="message-col message-col370">
                <div class="image">
                   <iframe width="370" height="220" src="//www.youtube.com/embed/Xzi7lTpNBEY" frameborder="0" allowfullscreen></iframe>            
                </div><!-- end image -->                
            </div><!-- end message-col -->
        </div><!-- end message-row -->
    </div><!-- end message-group -->
</div><!-- end primary-row -->

<?php get_template_part('part','flow'); ?>

<?php get_template_part('part','contact'); ?>

<div class="primary-row clearfix"><!-- begin primary-row -->
    <h2 class="h2_title">代表からの挨拶</h2>   
    <div class="message-right message-238 clearfix"><!-- begin message-238 -->
        <div class="image">
            <img src="<?php bloginfo('template_url'); ?>/img/content/top_content_img5.jpg" alt="top" />
        </div>
        <div class="text ln15em">
			テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
			テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
        </div>
    </div><!-- end message-238 -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->	
	<div class="top-facebook">
		<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2F%25E6%25A0%25AA%25E5%25BC%258F%25E4%25BC%259A%25E7%25A4%25BE%25E3%2583%2590%25E3%2583%25BC%25E3%2583%258B%25E3%2583%2583%25E3%2582%25B7%25E3%2583%25A5%2F714797041931821&amp;width=750&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:220px;" allowTransparency="true"></iframe>
	</div>	
</div><!-- end primary-row -->

<?php get_footer(); ?>