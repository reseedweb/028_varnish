<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2_title">お客様の声</h2>
	<p class="mb20">ビル&マンション清掃.comで清掃サービスをご利用いただいたお客様の生の声をご紹介いたします。</p>
	<h2 class="main-title">A市清掃作業</h2>	
	<div class="kuutyou-content-info2"><!-- begin kuutyou-content-info2 -->		
		<p class="text-right pb20">2014年12月11日（金）</p>	
		<p class="pb40">
			<img src="<?php bloginfo('template_url'); ?>/img/content/voice_content_img1.jpg" alt="voice" />
		</p>			
	</div><!-- end kuutyou-content-info2 -->	
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="main-title">B市清掃作業</h2>		
	<div class="kuutyou-content-info2"><!-- begin kuutyou-content-info2 -->		
		<p class="text-right pb20">2014年12月11日（金）</p>	
		<p class="pb40">
			<img src="<?php bloginfo('template_url'); ?>/img/content/voice_content_img2.jpg" alt="voice" />
		</p>			
	</div><!-- end kuutyou-content-info2 -->	
</div><!-- end primary-row -->

<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>