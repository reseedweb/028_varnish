<?php if(false): ?>
<nav class="globalNavi-static">
    <div class="container"><!-- begin container -->
        <div class="row clearfix"><!-- begin row -->
            <div class="col-md-18"><!-- begin col -->
            	<div class="globalNavi-content">					
            		<ul>
            			<li><a href="<?php bloginfo('url'); ?>/about">
            				<img src="<?php bloginfo('template_url'); ?>/img/common/globalNavi_img1.jpg" alt="about" />
            			</a></li><!-- end li -->
            			<li><a href="<?php bloginfo('url'); ?>/about">
            				<img src="<?php bloginfo('template_url'); ?>/img/common/globalNavi_img2.jpg" alt="about" />
            			</a></li><!-- end li -->
            			<li><a href="<?php bloginfo('url'); ?>/about">
            				<img src="<?php bloginfo('template_url'); ?>/img/common/globalNavi_img3.jpg" alt="about" />
            			</a></li><!-- end li -->
            			<li><a href="<?php bloginfo('url'); ?>/about">
            				<img src="<?php bloginfo('template_url'); ?>/img/common/globalNavi_img4.jpg" alt="about" />
            			</a></li><!-- end li -->
            			<li><a href="<?php bloginfo('url'); ?>/about">
            				<img src="<?php bloginfo('template_url'); ?>/img/common/globalNavi_img5.jpg" alt="about" />
            			</a></li><!-- end li -->            			            			            			            			
            		</ul>
				</div>                
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->	
</nav>
<?php endif; ?>
<nav class="globalNavi-dynamic">
    <div class="container"><!-- begin container -->
        <div class="row clearfix"><!-- begin row -->
            <div class="col-md-18"><!-- begin col -->
            	<div class="globalNavi-content">					
            		<ul class="globalNavi-content">
            			<li>
							<a href="<?php bloginfo('url'); ?>/">トップページ</a>
						</li>
            			<li>
							<a href="<?php bloginfo('url'); ?>/house">
								各種サービス
							</a>
							<ul>
								<li>
									<a href="<?php bloginfo('url'); ?>/yukamen">
										床面清掃
									</a>
								</li>
								<li>
									<a href="<?php bloginfo('url'); ?>/aircon">                                    
										エアコンクリーニング
									</a>
								</li>   
								<li>
									<a href="<?php bloginfo('url'); ?>/kuutyou">                                    
										空調設備管理
									</a>
								</li>
								<li>
									<a href="<?php bloginfo('url'); ?>/house">                                    
										ハウスクリーニング
									</a>
								</li>								
							</ul>
						</li>
						<li>
							<a href="<?php bloginfo('url'); ?>/price">参考価格例</a>
						</li>
						<li>
							<a href="<?php bloginfo('url'); ?>/flow">清掃の流れ</a>
						</li>
						<li>
							<a href="<?php bloginfo('url'); ?>/faq">よくあるご質問</a>
						</li>
						<li>
							<a href="<?php bloginfo('url'); ?>/company">会社概要</a>
						</li>												
            		</ul>					
					<div class="global-navi-space">
						<img src="<?php bloginfo('template_url'); ?>/img/common/global_navi_space.jpg" alt="globalnavi" />
					</div>
				</div>                
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
	<script language="javascript">
	$(document).ready(function(){
	    var globalNavi_width = parseInt( $('.globalNavi-dynamic .globalNavi-content').css('width') );   
	    var globalNavi_items = parseInt( $('.globalNavi-dynamic .globalNavi-content > ul > li').size() );  
	    $('.globalNavi-dynamic .globalNavi-content > ul > li').css('width', eval(globalNavi_width/globalNavi_items) + 'px' );
	});
	</script>     
</nav>