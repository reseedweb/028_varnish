<div class="sidebar-row pb20 clearfix"><!-- begin sidebar-row -->
    <div class="side-con"><!-- begin side-con -->
        <img alt="contact bg" 
        src="<?php bloginfo('template_url');?>/img/common/side_con_bg.jpg" />
        <div class="side-con-btn">
            <a href="<?php bloginfo('url'); ?>/contact">
                <img alt="contact" 
                src="<?php bloginfo('template_url');?>/img/common/side_con_btn.jpg" />
            </a>
        </div>
    </div><!-- end side-con -->
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
    <?php get_template_part('part','sideNavi'); ?>
</div><!-- end sidebar-row -->

<div class="sidebar-row pt20 clearfix"><!-- begin sidebar-row -->
    <a href="<?php bloginfo('url'); ?>/beforeafter">
        <img alt="side bnr" 
        src="<?php bloginfo('template_url');?>/img/common/side_befaft.jpg" />                         
    </a>
</div><!-- end sidebar-row -->

<div class="sidebar-row"><!-- begin sidebar-row -->
    <a href="<?php bloginfo('url'); ?>/blog">
        <img alt="side bnr" 
        src="<?php bloginfo('template_url');?>/img/common/side_blog.jpg" />                         
    </a>
</div><!-- end sidebar-row -->

<div class="sidebar-row"><!-- begin sidebar-row -->
    <a href="<?php bloginfo('url'); ?>/staff">
        <img alt="side bnr" 
        src="<?php bloginfo('template_url');?>/img/common/side_staff.jpg" />                         
    </a>
</div><!-- end sidebar-row -->

<div class="sidebar-row"><!-- begin sidebar-row -->
	<a href="https://www.facebook.com/pages/%E6%A0%AA%E5%BC%8F%E4%BC%9A%E7%A4%BE%E3%83%90%E3%83%BC%E3%83%8B%E3%83%83%E3%82%B7%E3%83%A5/714797041931821?ref=hl">
		<img alt="side bnr" src="<?php bloginfo('template_url');?>/img/common/side_fb.jpg" />                             
	</a>
</div><!-- end sidebar-row -->

<div class="sidebar-row"><!-- begin sidebar-row -->    
	<div class="side-map"><!-- begin side-con -->
       <img alt="side map" src="<?php bloginfo('template_url');?>/img/common/side_maps.jpg" />                             
        <div class="side-map-btn">
           <i class="fa fa-play"></i><a href="<?php bloginfo('url'); ?>/area">対応エリア一覧へ</a>
        </div>
    </div><!-- end side-con -->
</div><!-- end sidebar-row -->


<div class="sidebar-row clearfix">
	<div class="sideBlog">
		<h3 class="sideBlog-title">最新の投稿</h3>
		<ul>
			<?php query_posts("posts_per_page=5"); ?><?php if(have_posts()):while(have_posts()):the_post(); ?>
			<li>
			<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
			</li>
			<?php endwhile;endif; ?>
		</ul> 
	</div>
	<div class="sideBlog">
	    <h3 class="sideBlog-title">最新の投稿</h3>
	      <ul class="category">
			<?php wp_list_categories('sort_column=name&optioncount=0&hierarchical=1&title_li='); ?>
	      </ul>                             	
	</div>
	<div class="sideBlog">
      <h3 class="sideBlog-title">最新の投稿</h3>
      <ul><?php wp_get_archives('type=monthly&limit=12'); ?></ul>                               	
	</div>
</div>
