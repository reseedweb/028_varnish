<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2_title">清掃作業例</h2>
	<p class="mb20">ビル&マンション清掃.comの清掃作業例をご紹介いたします。</p>
	<h2 class="main-title">A市清掃作業</h2>	
	<div class="kuutyou-content-info2"><!-- begin kuutyou-content-info2 -->		
		<p class="text-right pb20">2014年12月11日（金）</p>	
		<p>
			<img src="<?php bloginfo('template_url'); ?>/img/content/sagyourei_content_img1.jpg" alt="sagyourei" />
		</p>
		<p class="pt20 pb20">
			清掃Before写真
		</p>
		<p>
			<img src="<?php bloginfo('template_url'); ?>/img/content/sagyourei_content_img2.jpg" alt="sagyourei" />
		</p>
		<p class="pt20 pb20">清掃After写真</p>		
	</div><!-- end kuutyou-content-info2 -->	
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="main-title">B市清掃作業</h2>	
	<div class="kuutyou-content-info2"><!-- begin kuutyou-content-info2 -->		
		<p class="text-right pb20">2014-10-24（金） |　店舗清掃ブログ</p>	
		<p>
			<img src="<?php bloginfo('template_url'); ?>/img/content/sagyourei_content_img3.jpg" alt="sagyourei" />
		</p>
		<p class="pt20 pb20">
			清掃Before写真
		</p>
		<p>
			<img src="<?php bloginfo('template_url'); ?>/img/content/sagyourei_content_img4.jpg" alt="sagyourei" />
		</p>
		<p class="pt20 pb20">清掃After写真</p>		
	</div><!-- end kuutyou-content-info2 -->	
</div><!-- end primary-row -->

<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>