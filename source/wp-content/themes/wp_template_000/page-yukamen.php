<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="table1-content-info">
		<img src="<?php bloginfo('template_url'); ?>/img/content/yukamen_content_img1.jpg" alt="yukamen" />
		<table class="info-text">
			<tr>
				<th>床面洗浄のみ</th>
				<td><span class="size">100m<sup>2</sup></span>8,000円～</td>
			</tr>
			<tr>
				<th>床面洗浄+ワックスがけ</th>
				<td><span class="size">100m<sup>2</sup></span>13,000円～</td>
			</tr>
			<tr>
				<th>カーペット洗浄</th>
				<td><span class="size">100m<sup>2</sup></span>18,000円～</td>
			</tr>
		</table>
	</div>
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="main-title">床面洗浄</h2>	
	<div class="kuutyou-content-info2"><!-- begin kuutyou-content-info2 -->		
		<div class="message-right message-323 clearfix"><!-- begin message-323 -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/yukamen_content_img2.jpg" alt="yukamen" />
			</div><!-- end image -->
			<div class="text ln15em">
				<p>最近のコンビニなどの床は、ワックスがけが必要ないメンテナンスフリーの床が多くなってきましたが、これらの床材に新しく貼り変えると金額がはってしまいます。「そんなに経費をかけられないけど、床をきれいにしたい…。」そんなお悩みはビル&マンション清掃.comが解決いたします！<br />
				私共の経験から培った確かな技術をもとに、お客様にご満足のいただける清掃サービスをご提供いたします。<br />
				その他のサービスとして、「排気ダクト清掃」「貯水槽清掃」「排水管洗浄」なども行っておりますので、お気軽にお問い合わせください。
				</p>
			</div><!-- end text -->
		</div><!-- end message-323 -->		
	</div><!-- end kuutyou-content-info2 -->	
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2_title">床面洗浄の作業手順</h2>
	<div class="message-group message-classic clearfix"><!-- begin message-group -->
        <div class="message-row clearfix"><!--message-row -->
            <div class="message-col message-col243 clearfix"><!-- begin message-col243 -->
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">
						<span class="step">Step1</span>
						<h3 class="title-info">片付け</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/yukamen_content_img3.jpg" alt="yukamen" /></p>
					<div class="info3-text ln15em">
						<p>(丁寧に掃除機をかけていきます。<br />
						広範囲の場合、ダスタークロスというモップを使います。) 
						</p>
					</div>
				</div>								
            </div><!-- end message-col243 -->            
						
			<div class="message-col message-col243 kuutyou-box clearfix"><!-- begin message-col243 -->
				<div class="kuutyou-arrow">
					<img src="<?php bloginfo('template_url'); ?>/img/content/main_content_arrrow.png" alt="yukamen" />
				</div>
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">
						<span class="step">Step2</span>
						<h3 class="title-info">モップがけ</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/yukamen_content_img4.jpg" alt="yukamen" /></p>
					<div class="info3-text ln15em">
						<p>ダスタークロスというモップで 丁寧にほこりなどを集め、掃除 機で吸い取ります</p>
					</div>
				</div> 			
            </div><!-- end message-col243 -->			
			<div class="message-col message-col243 kuutyou-box clearfix"><!-- begin message-col243 -->
				<div class="kuutyou-arrow">
					<img src="<?php bloginfo('template_url'); ?>/img/content/main_content_arrrow.png" alt="yukamen" />
				</div>
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">
						<span class="step">Step3</span>
						<h3 class="title-info">洗剤の塗布</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/yukamen_content_img5.jpg" alt="yukamen" /></p>
					<div class="info3-text ln15em">
						<p>モップを使い床面に専用の洗剤<br />
						を塗っていきます。塗り終わったら少しの時間乾かします。</p>
					</div>
				</div>                
            </div><!-- end message-col243 -->
        </div><!-- end message-row -->
		
		<div class="message-row clearfix"><!--message-row -->
            <div class="message-col message-col243 clearfix"><!-- begin message-col243 -->
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">
						<span class="step">Step4</span>
						<h3 class="title-info">専用の機械で洗浄</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/yukamen_content_img6.jpg" alt="yukamen" /></p>
					<div class="info3-text ln15em">
						<p>ポリッシャーという床洗浄の機<br />
						械で洗い、汚れを丁寧に落としていきます。</p>
					</div>
				</div>								
            </div><!-- end message-col243 -->            
						
			<div class="message-col message-col243 kuutyou-box clearfix"><!-- begin message-col243 -->
				<div class="kuutyou-arrow">
					<img src="<?php bloginfo('template_url'); ?>/img/content/main_content_arrrow.png" alt="yukamen" />
				</div>
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">
						<span class="step">Step5</span>
						<h3 class="title-info">水拭き</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/yukamen_content_img7.jpg" alt="kuutyou" /></p>
					<div class="info3-text ln15em">
						<p>モップを使い、ていねいに水拭<br />
						きをします。その後、乾いたモップで乾拭きをします。</p>
					</div>
				</div> 			
            </div><!-- end message-col243 -->			
			<div class="message-col message-col243 kuutyou-box clearfix"><!-- begin message-col243 -->
				<div class="kuutyou-arrow">
					<img src="<?php bloginfo('template_url'); ?>/img/content/main_content_arrrow.png" alt="yukamen" />
				</div>
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">
						<span class="step">Step6</span>
						<h3 class="title-info">乾燥</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/yukamen_content_img8.jpg" alt="yukamen" /></p>
					<div class="info3-text ln15em">
						<p>送風機を使い、いったん床面を<br />
						乾燥させます。エアコンを使用するのも効果的です。</p>
					</div>
				</div>                
            </div><!-- end message-col243 -->
        </div><!-- end message-row -->
		
		<div class="message-row clearfix"><!--message-row -->
            <div class="message-col message-col243 clearfix"><!-- begin message-col243 -->
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">
						<span class="step">Step7</span>
						<h3 class="title-info">ワックス塗布</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/yukamen_content_img9.jpg" alt="yukamen" /></p>
					<div class="info3-text ln15em">
						<p>床面が乾燥したら、ワックスを塗ります。<br />
						ワックスは通h常２回塗ります。</p>
					</div>
				</div>								
            </div><!-- end message-col243 -->            
						
			<div class="message-col message-col243 kuutyou-box clearfix"><!-- begin message-col243 -->
				<div class="kuutyou-arrow">
					<img src="<?php bloginfo('template_url'); ?>/img/content/main_content_arrrow.png" alt="yukamen" />
				</div>
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">
						<span class="step">Step8</span>
						<h3 class="title-info">乾燥</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/yukamen_content_img10.jpg" alt="kuutyou" /></p>
					<div class="info3-text ln15em">
						<p>ワックスを塗り終えたら、再度<br />
						しっかり乾燥させます。</p>
					</div>
				</div> 			
            </div><!-- end message-col243 -->			
			<div class="message-col message-col243 kuutyou-box clearfix"><!-- begin message-col243 -->
				<div class="kuutyou-arrow">
					<img src="<?php bloginfo('template_url'); ?>/img/content/main_content_arrrow.png" alt="yukamen" />
				</div>
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">
						<span class="step">Step9</span>
						<h3 class="title-info">完成</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/yukamen_content_img11.jpg" alt="yukamen" /></p>
					<div class="info3-text ln15em">
						<p>すっかりキレイになりました。</p>
					</div>
				</div>                
            </div><!-- end message-col243 -->
        </div><!-- end message-row -->
    </div><!-- end message-group -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2_title">床面洗浄の料金</h2>
	<table class="table2-content-info">
		<tr>
			<th>床面洗浄のみ</th>
			<td><span class="size">100m<sup>2</sup></span>8,000円～</td>
		</tr>
		<tr>
			<th>床面洗浄＋ワックスがけ</th>
			<td><span class="size">100m<sup>2</sup></span>13,000円～</td>
		</tr>
		<tr>
			<th>床面はくり+ワックスがけ</th>
			<td><span class="size">100m<sup>2</sup></span>20,000円～</td>
		</tr>
		<tr>
			<th>カーペット洗浄</th>
			<td><span class="size">100m<sup>2</sup></span>18,000円～</td>
		</tr>
	</table>
</div><!-- end primary-row -->


<?php get_template_part('part','flow'); ?>
<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>