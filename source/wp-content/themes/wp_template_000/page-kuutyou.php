<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="kuutyou-content-info1">
		<img src="<?php bloginfo('template_url'); ?>/img/content/kuutyou_content_img1.jpg" alt="kuutyou" />
		<table class="info1-text">
			<tr>
				<th>空調設備フィルター交換・清掃</th>
				<td>12,000円～</td>
			</tr>
			<tr>
				<th>排気ファン等のベルト交換</th>
				<td>11,000円～</td>
			</tr>
			<tr>
				<th>クーリングタワー清掃</th>
				<td>10,000円～</td>
			</tr>
		</table>
	</div>
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="main-title">空調設備管理</h2>	
	<div class="kuutyou-content-info2"><!-- begin kuutyou-content-info2 -->
		<h3 class="h3-title mb20">空調設備フィルター交換・清掃</h3>
		<div class="message-right message-323 clearfix"><!-- begin message-323 -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/kuutyou_content_img2.jpg" alt="kuutyou" />
			</div><!-- end image -->
			<div class="text ln15em">
				<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
				<p class="pt20">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキス</p>
			</div><!-- end text -->
		</div><!-- end message-323 -->
		
		<h3 class="h3-title mt20 mb20">排気ファン等のベルト交換・清掃</h3>
		<div class="message-right message-323 clearfix"><!-- begin message-323 -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/kuutyou_content_img3.jpg" alt="kuutyou" />
			</div><!-- end image -->
			<div class="text ln15em">
				<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
				<p class="pt20">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキス</p>
			</div><!-- end text -->
		</div><!-- end message-323 -->
		
		<h3 class="h3-title mt20 mb20">クーリングタワー清掃</h3>
		<div class="message-right message-323 clearfix"><!-- begin message-323 -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/kuutyou_content_img4.jpg" alt="kuutyou" />
			</div><!-- end image -->
			<div class="text ln15em">
				<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
				<p class="pt20">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキス</p>
			</div><!-- end text -->
		</div><!-- end message-323 -->
	</div><!-- end kuutyou-content-info2 -->	
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2_title">空調設備洗浄の作業手順</h2>
	<div class="message-group message-classic clearfix"><!-- begin message-group -->
        <div class="message-row clearfix"><!--message-row -->
            <div class="message-col message-col243 clearfix"><!-- begin message-col243 -->
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">
						<span class="step">Step1</span>
						<h3 class="title-info">汚れている状態です</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/kuutyou_content_img5.jpg" alt="kuutyou" /></p>
					<div class="info3-text ln15em">
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
					</div>
				</div>								
            </div><!-- end message-col243 -->            
						
			<div class="message-col message-col243 kuutyou-box clearfix"><!-- begin message-col243 -->
				<div class="kuutyou-arrow">
					<img src="<?php bloginfo('template_url'); ?>/img/content/main_content_arrrow.png" alt="kuutyou" />
				</div>
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">
						<span class="step">Step2</span>
						<h3 class="title-info">デッキブラシでこする</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/kuutyou_content_img6.jpg" alt="kuutyou" /></p>
					<div class="info3-text ln15em">
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
					</div>
				</div> 			
            </div><!-- end message-col243 -->			
			<div class="message-col message-col243 kuutyou-box clearfix"><!-- begin message-col243 -->
				<div class="kuutyou-arrow">
					<img src="<?php bloginfo('template_url'); ?>/img/content/main_content_arrrow.png" alt="kuutyou" />
				</div>
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">
						<span class="step">Step3</span>
						<h3 class="title-info">きれいになりました</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/kuutyou_content_img7.jpg" alt="kuutyou" /></p>
					<div class="info3-text ln15em">
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
					</div>
				</div>                
            </div><!-- end message-col243 -->
        </div><!-- end message-row -->
    </div><!-- end message-group -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2_title">空調設備洗浄のの料</h2>
	<table class="kuutyou-content-info4">
		<tr>
			<th>空調設備フィルター交換</th>
			<td>12,000円～</td>
		</tr>
		<tr>
			<th>排気ファンなどのベルト交換</th>
			<td>11,000円～</td>
		</tr>
		<tr>
			<th>クーリングタワー清掃</th>
			<td>10,000円～</td>
		</tr>
	</table>
</div><!-- end primary-row -->

<?php get_template_part('part','flow'); ?>
<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>