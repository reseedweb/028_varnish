<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="table1-content-info">
		<img src="<?php bloginfo('template_url'); ?>/img/content/aircon_content_img1.jpg" alt="aircon" />
		<table class="info-text">
			<tr>
				<th>一般家庭用ルームエアコン</th>
				<td><span class="size">1台</span>8,000円～</td>
			</tr>
			<tr>
				<th>業務用ルームエアコン</th>
				<td><span class="size">1台</span>23,000円～</td>
			</tr>
			<tr>
				<th>室外機</th>
				<td><span class="size">1台</span>4,000円～</td>
			</tr>
		</table>
	</div>
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="main-title">エアコンクリーニング</h2>	
	<div class="kuutyou-content-info2"><!-- begin kuutyou-content-info2 -->		
		<div class="message-right message-323 clearfix"><!-- begin message-323 -->
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/aircon_content_img2.jpg" alt="aircon" />
			</div><!-- end image -->
			<div class="text ln15em">
				<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
				<p class="pt20">テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキス</p>
			</div><!-- end text -->
		</div><!-- end message-323 -->		
	</div><!-- end kuutyou-content-info2 -->	
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2_title">エアコンクリーニングの作業手順</h2>
	<div class="message-group message-classic clearfix"><!-- begin message-group -->
        <div class="message-row clearfix"><!--message-row -->
            <div class="message-col message-col243 clearfix"><!-- begin message-col243 -->
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">
						<span class="step">Step1</span>
						<h3 class="title-info">部品を外す</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/aircon_content_img3.jpg" alt="aircon" /></p>
					<div class="info3-text ln15em">
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
					</div>
				</div>								
            </div><!-- end message-col243 -->            
						
			<div class="message-col message-col243 kuutyou-box clearfix"><!-- begin message-col243 -->
				<div class="kuutyou-arrow">
					<img src="<?php bloginfo('template_url'); ?>/img/content/main_content_arrrow.png" alt="aircon" />
				</div>
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">
						<span class="step">Step2</span>
						<h3 class="title-info">汚れを確認</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/aircon_content_img4.jpg" alt="aircon" /></p>
					<div class="info3-text ln15em">
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
					</div>
				</div> 			
            </div><!-- end message-col243 -->			
			<div class="message-col message-col243 kuutyou-box clearfix"><!-- begin message-col243 -->
				<div class="kuutyou-arrow">
					<img src="<?php bloginfo('template_url'); ?>/img/content/main_content_arrrow.png" alt="aircon" />
				</div>
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">
						<span class="step">Step3</span>
						<h3 class="title-info">養生シート</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/aircon_content_img5.jpg" alt="aircon" /></p>
					<div class="info3-text ln15em">
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
					</div>
				</div>                
            </div><!-- end message-col243 -->
        </div><!-- end message-row -->
		
		<div class="message-row clearfix"><!--message-row -->
            <div class="message-col message-col243 clearfix"><!-- begin message-col243 -->
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">
						<span class="step">Step4</span>
						<h3 class="title-info">専用の機械で洗浄</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/aircon_content_img6.jpg" alt="aircon" /></p>
					<div class="info3-text ln15em">
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
					</div>
				</div>								
            </div><!-- end message-col243 -->            
						
			<div class="message-col message-col243 kuutyou-box clearfix"><!-- begin message-col243 -->
				<div class="kuutyou-arrow">
					<img src="<?php bloginfo('template_url'); ?>/img/content/main_content_arrrow.png" alt="aircon" />
				</div>
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">
						<span class="step">Step5</span>
						<h3 class="title-info">部品を洗う</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/aircon_content_img7.jpg" alt="aircon" /></p>
					<div class="info3-text ln15em">
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
					</div>
				</div> 			
            </div><!-- end message-col243 -->			
			<div class="message-col message-col243 kuutyou-box clearfix"><!-- begin message-col243 -->
				<div class="kuutyou-arrow">
					<img src="<?php bloginfo('template_url'); ?>/img/content/main_content_arrrow.png" alt="aircon" />
				</div>
				<div class="kuutyou-content-info3 clearfix">
					<div class="info3-title clearfix">
						<span class="step">Step6</span>
						<h3 class="title-info">作業完了</h3>
					</div>
					<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/aircon_content_img8.jpg" alt="aircon" /></p>
					<div class="info3-text ln15em">
						<p>テキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
					</div>
				</div>                
            </div><!-- end message-col243 -->
        </div><!-- end message-row -->
    </div><!-- end message-group -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2_title">エアコンクリーニングの料金</h2>
	<table class="table2-content-info">
		<tr>
			<th>一般家庭用ルームエアコン</th>
			<td><span class="size">1台</span>8,000円～</td>
		</tr>
		<tr>
			<th>業務用ルームエアコン</th>
			<td><span class="size">1台</span>23,000円～</td>
		</tr>
		<tr>
			<th>室外機</th>
			<td><span class="size">1台</span>4,000円～</td>
		</tr>
	</table>
</div><!-- end primary-row -->


<?php get_template_part('part','flow'); ?>
<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>