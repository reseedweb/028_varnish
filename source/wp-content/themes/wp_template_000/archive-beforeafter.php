<?php get_header(); ?>
	<div class="primary-row clearfix">
		<div id="beforeafter-detail-navi">
	        <ul class="beforeafter-detail-navi clearfix">
				<li><a href="<?php bloginfo('url'); ?>/cat-beforeafter/床面清掃">床面清掃</a></li>
	            <li><a href="<?php bloginfo('url'); ?>/cat-beforeafter/エアコンクリーニング">エアコンクリーニング</a></li>
	            <li><a href="<?php bloginfo('url'); ?>/cat-beforeafter/空調設備管理">空調設備管理</a></li>
	            <li><a href="<?php bloginfo('url'); ?>/cat-beforeafter/ハウスクリーニング">ハウスクリーニング</a></li>
	            <li><a href="<?php bloginfo('url'); ?>/cat-beforeafter/その他">その他</a></li>                                                  
	        </ul>	
		</div>
	</div>
		<?php
		$queried_object = get_queried_object();	 
	    ?>
	    <?php $posts = get_posts(array(
	        'post_type'=> 'beforeafter',
			'posts_per_page' => get_query_var('posts_per_page'),
			'paged' => get_query_var('paged'),
	    ));		
	    ?>		
	    <div class="message-group clearfix">
	    <?php $i = 0; ?>
	    <?php foreach($posts as $p) :  ?>	  
			<?php $i++; ?>
	        <?php if($i%2 == 1) : ?>
	        <div class="message-row clearfix">
	        <?php endif; ?>			      
				<div class="message-col message-col370 archive-beforeafter">			
					<div class="beforeafter-title"> <a href="<?php echo get_the_permalink($p->ID); ?>"><?php echo $p->post_title; ?></a></div>
					 <div class="image">
						<a href="<?php echo get_the_permalink($p->ID); ?>">
							<?php echo get_the_post_thumbnail( $p->ID,'small'); ?>                    			                    
						</a>																
					</div>															
					<div class="pt10">
						<div class="beforeafter-category"><?php @the_terms($p->ID, 'cat-beforeafter'); ?></div>
						<div class="beforeafter-date"><?php echo get_the_date('Y/m/d', $p->ID); ?></div>
					</div>					        	            					
				</div>
				<?php if($i%2 == 0 || $i == count($posts) ) : ?>
	        </div>
	        <?php endif; ?>
	    <?php endforeach; ?>		
	    </div>
		<div class="primary-row voice-page">
	        <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
	    </div>
	    <?php wp_reset_query(); ?>    		

	<?php get_template_part('part','flow'); ?>
    <?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>