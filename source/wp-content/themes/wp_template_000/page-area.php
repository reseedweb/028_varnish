<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="primary-row clearfix"><!-- primary-row -->
		<h2 class="h2_title">対応エリア一覧</h2>
		<div id="area_content">
			<p><strong>大阪</strong></p>
			<dl>
				<dt>【大阪市】</dt>
					<dd><?php $loop = new WP_Query(array("post_type" => "osakashi", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					<?php endwhile; ?></dd>

				<dt>【北大阪】</dt>
					<dd><?php $loop = new WP_Query(array("post_type" => "kitaosaka", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					<?php endwhile; ?></dd>

				<dt>【東大阪】</dt>
					<dd><?php $loop = new WP_Query(array("post_type" => "higashiosaka", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					<?php endwhile; ?></dd>



				<dt>【南河内】</dt>
					<dd><?php $loop = new WP_Query(array("post_type" => "minamikawati", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					<?php endwhile; ?>
				</dd>


				<dt>【泉州】</dt>
					<dd><?php $loop = new WP_Query(array("post_type" => "sensyuu", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					<?php endwhile; ?>
			</dd>
		</dl>
		<dl>
			<dt><span>奈良県</span>　一部地域</dt>
					<dd><?php $loop = new WP_Query(array("post_type" => "nara", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					<?php endwhile; ?></dd>

					<dt><span>兵庫県</span>　一部地域</dt>
					<dd><?php $loop = new WP_Query(array("post_type" => "hyogo", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					<?php endwhile; ?></dd>

					<dt><span>京都府</span>　一部地域</dt>
					<dd><?php $loop = new WP_Query(array("post_type" => "kyoto", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					<?php endwhile; ?></dd>

					<dt><span>和歌山県</span>　一部地域</dt>
					<dd><?php $loop = new WP_Query(array("post_type" => "wakayama", "posts_per_page" => 30 ));
					while($loop->have_posts()): $loop->the_post(); ?>
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					<?php endwhile; ?></dd>

		</dl>
		</div>
</div><!-- end primary-row -->		
<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>