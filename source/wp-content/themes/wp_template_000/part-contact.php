<div class="primary-row clearfix">
	<div class="part-contact">
		<img src="<?php bloginfo('template_url'); ?>/img/common/part_con_bg.jpg" alt="contact" />
		<div class="part-contact-btn">
			<a href="<?php bloginfo('url'); ?>/contact">
				<img src="<?php bloginfo('template_url'); ?>/img/common/part_con_btn.jpg" alt="contact" />
			</a>
		</div>
		<div class="text">
			お電話でもメールからでもお気軽にお問い合わせください。<br />
			お客様からよく寄せられるご質問を<span class="text-note">こちら</span>で紹介しておりますので是非参照ください。
		</div>
	</div>
</div>