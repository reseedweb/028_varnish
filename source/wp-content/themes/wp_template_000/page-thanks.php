<?php get_header(); ?>	
    <div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">送信完了しました</span></h2>
		<p class="ln15em">
			お問い合わせいただきありがとうございました。<br />
			＊送信していただきましたメールは確認し次第、追ってご連絡させていただきます。<br />
			＊確認にお時間を要する場合がございますので、あらかじめご了承願います。
		</p>
    </div><!-- end primary-row -->
<?php get_footer(); ?>