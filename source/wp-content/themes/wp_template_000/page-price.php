<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2_title">床面洗浄</h2>
	<h3 class="main-title">床面洗浄のみ</h3>	
	<table class="price-info"><!-- begin price-info -->
		<tr>
			<th>100 ㎡</th>
			<td>￥8,000〜<span class="price-vat">(税別)</span></td>
		</tr>
		<tr>
			<th>200 ㎡</th>
			<td>￥16,000〜<span class="price-vat">(税別)</span></td>
		</tr>
		<tr>
			<th>300 ㎡〜</th>
			<td>お問い合わせ下さい。</td>
		</tr>
	</table><!-- end price-info -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->	
	<h3 class="main-title">床面洗浄+ワックスがけ</h3>	
	<table class="price-info"><!-- begin price-info -->
		<tr>
			<th>100 ㎡</th>
			<td>￥13,000〜<span class="price-vat">(税別)</span></td>
		</tr>
		<tr>
			<th>200 ㎡</th>
			<td>￥26,000〜<span class="price-vat">(税別)</span></td>
		</tr>
		<tr>
			<th>300 ㎡〜</th>
			<td>お問い合わせ下さい。</td>
		</tr>
	</table><!-- end price-info -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->	
	<h3 class="main-title">カーペット洗浄</h3>	
	<table class="price-info mb20"><!-- begin price-info -->
		<tr>
			<th>100 ㎡</th>
			<td>￥18,000〜<span class="price-vat">(税別)</span></td>
		</tr>
		<tr>
			<th>200 ㎡</th>
			<td>￥36,000〜<span class="price-vat">(税別)</span></td>
		</tr>
		<tr>
			<th>300 ㎡〜</th>
			<td>お問い合わせ下さい。</td>
		</tr>
	</table><!-- end price-info -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->	
	<h2 class="h2_title">エアコンクリーニング</h2>
	<h3 class="main-title">一般家庭用ルームエアコンクリーニング</h3>	
	<table class="price-info"><!-- begin price-info -->
		<tr>
			<th>1台</th>
			<td>￥8,000〜<span class="price-vat">(税別)</span></td>
		</tr>		
	</table><!-- end price-info -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->	
	<h3 class="main-title">業務用エアコンクリーニング</h3>		
	<table class="price-info"><!-- begin price-info -->
		<tr>
			<th>1基</th>
			<td>￥23,000〜<span class="price-vat">(税別)</span></td>
		</tr>		
	</table><!-- end price-info -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->		
	<h3 class="main-title">室外機クリーニング</h3>	
	<table class="price-info mb20"><!-- begin price-info -->
		<tr>
			<th>1台</th>
			<td>￥4,000〜<span class="price-vat">(税別)</span></td>
		</tr>		
	</table><!-- end price-info -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->		
	<h2 class="h2_title">空調設備</h2>
	<h3 class="main-title">空調設備フィルター交換・清掃</h3>	
	<table class="price-info"><!-- begin price-info -->
		<tr>
			<th>1台</th>
			<td>￥10,000〜<span class="price-vat">(税別)</span></td>
		</tr>		
	</table><!-- end price-info -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->			
	<h3 class="main-title">排気ファン等のベルト交換</h3>	
	<table class="price-info"><!-- begin price-info -->
		<tr>
			<th>1基</th>
			<td>￥11,000〜<span class="price-vat">(税別)</span></td>
		</tr>		
	</table><!-- end price-info -->
</div><!-- end primary-row -->

<div class="primary-row learfix"><!-- begin primary-row -->			
	<h3 class="main-title">クーリングタワー清掃</h3>	
	<table class="price-info mb20"><!-- begin price-info -->
		<tr>
			<th>1台</th>
			<td>￥12,000〜<span class="price-vat">(税別)</span></td>
		</tr>		
	</table><!-- end price-info -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->		
	<h2 class="h2_title">ハウスクリーニング</h2>
	<h3 class="main-title">ハウスクリーニング</h3>	
	<table class="price-info"><!-- begin price-info -->
		<tr>
			<th>1ルーム</th>
			<td>￥8,000〜<span class="price-vat">(税別)</span></td>
		</tr>	
		<tr>
			<th>1LDK</th>
			<td>￥20,000〜<span class="price-vat">(税別)</span></td>
		</tr>
		<tr>
			<th>2LDK</th>
			<td>￥25,000〜<span class="price-vat">(税別)</span></td>
		</tr>
		<tr>
			<th>3LDK</th>
			<td>￥35,000〜<span class="price-vat">(税別)</span></td>
		</tr>
		<tr>
			<th>4LDK</th>
			<td>￥45,000〜<span class="price-vat">(税別)</span></td>
		</tr>
	</table><!-- end price-info -->
</div><!-- end primary-row -->
<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>
