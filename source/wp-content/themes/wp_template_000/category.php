<?php get_header(); ?>                  
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">ブログ</h2>
		<p>清掃内容やスタッフの日常などをご紹介いたします。</p>
	</div><!-- end primary-row -->
	<?php
		$queried_object = get_queried_object();
		$term_id = $queried_object->term_id;
		//print_r($queried_object);
		?>
		<?php $posts = get_posts(array(
			'post_type'=> 'post',
			'posts_per_page' => get_query_var('posts_per_page'),
			'paged' => get_query_var('paged'),
				'tax_query' => array(
				array(
				'taxonomy' => $queried_object->taxonomy,
				'field' => 'term_id',
				'terms' => $term_id))
		));
    ?>    
    <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>    
        <!-- do stuff ... -->        
		<div class="primary-row clearfix"><!-- begin primary-row -->			
			<h2 class="main-title"><?php the_title(); ?></h2>	
			<div class="kuutyou-content-info2"><!-- begin kuutyou-content-info2 -->		
				<p class="text-right pb20"><?php the_time('l, F jS, Y'); ?></p>	
				<div class="clearfix">
					<?php the_content(); ?>
				</div>			
			</div><!-- end kuutyou-content-info2 -->	
		</div><!-- end primary-row -->
        <?php endwhile; ?>    
        <div class="primary-row">
            <?php if(function_exists('wp_pagenavi')) wp_pagenavi(); ?>
        </div>
    <?php endif; ?>
    <?php wp_reset_query(); ?>     
<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>