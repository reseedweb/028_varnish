<div class="headerbtn">
	<div class="container">
		<div class="row">
			<div class="col-md-18">
				<div class="headerbtn-content">
					<ul class="clearfix">
						<li>
							<a class="darktooltip" href="<?php bloginfo('url'); ?>/yukamen">
								<img src="<?php bloginfo('template_url'); ?>/img/top/header_btn1.jpg" alt="top" />
							</a>
						</li>
						<li>
							<a class="darktooltip" href="<?php bloginfo('url'); ?>/aircon">
								<img src="<?php bloginfo('template_url'); ?>/img/top/header_btn2.jpg" alt="top" />
							</a>
						</li>
						<li>
							<a class="darktooltip" href="<?php bloginfo('url'); ?>/kuutyou">
								<img src="<?php bloginfo('template_url'); ?>/img/top/header_btn3.jpg" alt="top" />
							</a>
						</li>
						<li>
							<a class="darktooltip" href="<?php bloginfo('url'); ?>/house">
								<img src="<?php bloginfo('template_url'); ?>/img/top/header_btn4.jpg" alt="top" />
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>	
</div>