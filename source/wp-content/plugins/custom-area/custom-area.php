<?php
/*
Plugin Name: Custom Area
Plugin URI: 
Description: Custom Area. Use : CustomArea::view();
Author: Nguyen Minh Son
Author URI: 
Text Domain: custom-area
Domain Path: 
Version: 1.0
*/
/* Start Adding Functions Below this Line */
define('CUSTOM_AREA','1.0');
define('CUSTOM_AREA_PATH', dirname(__FILE__));

class CustomArea{
	private static $instance;
	public static function get_instance()
	{
        if ( null == self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
	}

	public function __construct()
	{
		add_action( 'init', array($this,'create_custom_post'), 0);
	}	

	function create_custom_post()
	{
		$this->init_post('osakashi','【大阪市】','【大阪市】','【大阪市】');
		$this->init_post('kitaosaka','【北大阪】','【北大阪】','【北大阪】');
		$this->init_post('higashiosaka','【東大阪】','【東大阪】','【東大阪】');
		$this->init_post('minamikawati','【南河内】','【南河内】','【南河内】');
		$this->init_post('sensyuu','【泉州】','【泉州】','【泉州】');
		$this->init_post('nara','奈良県','奈良県','奈良県');		
		$this->init_post('hyogo','兵庫県','兵庫県','兵庫県');		
		$this->init_post('kyoto','京都府','京都府','京都府');
		$this->init_post('wakayama','和歌山県','和歌山県','和歌山県');
	}

	public function init_post($post_type,$label,$all_label, $description) {

		$labels = array(
			'name'                => _x( $label, 'Post Type General Name', 'text_domain' ),
			'singular_name'       => _x( $label, 'Post Type Singular Name', 'text_domain' ),
			'menu_name'           => __( $label, 'text_domain' ),
			'parent_item_colon'   => __( $label . ' parent', 'text_domain' ),
			'all_items'           => __( 'All ' . $label, 'text_domain' ),
			'view_item'           => __( 'View ' ),
			'add_new_item'        => __( 'Add ' . $label, 'text_domain' ),
			'add_new'             => __( 'Add new ' . $label, 'text_domain' ),
			'edit_item'           => __( 'Edit ' . $label, 'text_domain' ),
			'update_item'         => __( 'Update ' . $label, 'text_domain' ),
			'search_items'        => __( 'Search ' . $label, 'text_domain' ),
			'not_found'           => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'  => __( 'Not found in trash', 'text_domain' ),
		);
		$args = array(
			'label'               => __( $all_label, 'text_domain' ),
			'description'         => __( $description, 'text_domain' ),
			'labels'              => $labels,
			'supports'            => array(  'title', 'editor' ),			
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			//'menu_position'       => 5,
			//'menu_icon'           => 'post',
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
		);
		register_post_type( $post_type, $args );
	}	

	public static function view()
	{
	?>
	<div class="eigyouarea">
	    <div>
	    <h4 class="sub-title">大阪</h4>
	    <dl>
	        <dt>大阪市</dt>
	        <dd>
	            <p><?php $loop = new WP_Query(array("post_type" => "osakashi", "posts_per_page" => 30 ));
	            while($loop->have_posts()): $loop->the_post(); ?>
	            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	            <?php endwhile; ?></p>
	        </dd>
	        <dt>北大阪</dt>
	        <dd>
	            <p><?php $loop = new WP_Query(array("post_type" => "kitaosaka", "posts_per_page" => 30 ));
	            while($loop->have_posts()): $loop->the_post(); ?>
	            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	            <?php endwhile; ?></p>
	        </dd>
	        <dt>東大阪</dt>
	        <dd>
	            <P><?php $loop = new WP_Query(array("post_type" => "higashiosaka", "posts_per_page" => 30 ));
	            while($loop->have_posts()): $loop->the_post(); ?>
	            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	            <?php endwhile; ?></P>
	        </dd>

	        <dt>南河内</dt>
	        <dd>
	            <p><?php $loop = new WP_Query(array("post_type" => "minamikawati", "posts_per_page" => 30 ));
	            while($loop->have_posts()): $loop->the_post(); ?>
	            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	            <?php endwhile; ?>
	            </p>
	        </dd>

	        <dt>泉州</dt>
	        <dd><p>
	            <?php $loop = new WP_Query(array("post_type" => "sensyuu", "posts_per_page" => 30 ));
	            while($loop->have_posts()): $loop->the_post(); ?>
	            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	            <?php endwhile; ?>
	        </p>
	        </dd>
	    </dl>
	    <div id="sub_area">
	    <h4 class="sub-title">奈良県</h4>
	        <p>
	            <?php $loop = new WP_Query(array("post_type" => "nara", "posts_per_page" => 30 ));
	            while($loop->have_posts()): $loop->the_post(); ?>
	            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	            <?php endwhile; ?>
	        </p>
	    <h4 class="sub-title">兵庫県</h4>
	        <p>
	            <?php $loop = new WP_Query(array("post_type" => "hyogo", "posts_per_page" => 30 ));
	            while($loop->have_posts()): $loop->the_post(); ?>
	            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	            <?php endwhile; ?>
	        </p>
	    <h4 class="sub-title">京都府</h4>
	        <p>
	            <?php $loop = new WP_Query(array("post_type" => "kyoto", "posts_per_page" => 30 ));
	            while($loop->have_posts()): $loop->the_post(); ?>
	            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	            <?php endwhile; ?>
	        </p>
	    <h4 class="sub-title">和歌山県</h4>
	        <p>
	            <?php $loop = new WP_Query(array("post_type" => "wakayama", "posts_per_page" => 30 ));
	            while($loop->have_posts()): $loop->the_post(); ?>
	            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	            <?php endwhile; ?>
	        </p>
	    </div>
	</div>
	<?php
	}
}
CustomArea::get_instance();